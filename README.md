## KASA - OpenClassroom

#### Le projet:

C'est une application web fabriqué en React, JavaScript, et CSS.
Le but était de développer une première application sous React en utilisant ses composants à partir d'une maquette et d'un fichier JSON.

## installations et instructions
 
Clonez ce repository. Vous allez avoir besoin de `npm` d'installé.  

installation:

`npm install`   

démarrer le serveur:

`npm start`  

Une page s'ouvrira dans votre navigateur comme suit:

`http://localhost:3000`  
