import React from "react";

import "./css/theme.css";

import Nav from "./components/Nav.jsx";
import Footer from "./components/Footer.jsx";

import Home from "./components/Home.jsx";
import Apropos from "./components/Apropos.jsx";
import FicheDetail from "./components/Fiche.jsx";
import NotFound from "./components/NotFound.jsx";

import {
    Redirect,
    BrowserRouter as Router,
    Route,
    Switch,
} from "react-router-dom";

function App() {
    return (
        <div className="App">
            <Router>
                <Nav />
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/apropos" component={Apropos} />
                    <Route exact path="/fiche/:id" component={FicheDetail} />
                    <Route path="/404" component={NotFound} />
                    <Redirect to="/404" />
                </Switch>
                <Footer />
            </Router>
        </div>
    );
}

export default App;
