export function ColapseClicked(elem) {
    const parent = elem.target.parentElement.parentElement.parentElement;

    if (parent.classList.contains("active")) {
        parent.classList.remove("active");
    } else {
        parent.classList.add("active");
    }
}
