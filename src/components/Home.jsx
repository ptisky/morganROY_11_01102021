import React from "react";
import bd from "../database/bd.json";
import { Link } from "react-router-dom";

const Home = () => {
    return (
        <div>
            <section id="header_homepage">
                <picture>
                    <img
                        src="/img/landscape.png"
                        className="background_header_hp"
                        alt="landscape"
                        title="landscape"
                    />
                </picture>

                <h2>Chez vous, partout et ailleurs</h2>
            </section>

            <section id="rent_list">
                {bd.map((item, key) => {
                    return (
                        <article key={item.id}>
                            <div className="container-rent">
                                <div className="img_container">
                                    <img src={item.cover} alt="item.title" />
                                </div>
                                <Link to={`/fiche/${item.id}`}>
                                    <h3>{item.title}</h3>
                                </Link>
                            </div>
                        </article>
                    );
                })}
            </section>
        </div>
    );
};

export default Home;
