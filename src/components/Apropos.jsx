import React from "react";
import { ColapseClicked } from "../js/Collapse.js";

const Apropos = () => {
    return (
        <div id="Apropos">
            <div className="img_container">
                <img
                    src="/img/landscape2.png"
                    className="background_header_hp"
                    alt="landscape2"
                    title="landscape2"
                />
            </div>

            <div className="collapse">
                <div className="collapse_title">
                    <span className="name">Fiabilité</span>
                    <span className="arrow" onClick={(e) => ColapseClicked(e)}>
                        <img src="/img/arrow.png" alt="arrow" />
                    </span>
                </div>

                <div className="content">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Etiam dignissim justo arcu, in elementum diam vulputate
                        in. Aliquam erat volutpat.
                    </p>
                </div>
            </div>

            <div className="collapse">
                <div className="collapse_title">
                    <span className="name">Respect</span>
                    <span className="arrow" onClick={(e) => ColapseClicked(e)}>
                        <img src="/img/arrow.png" alt="arrow" />
                    </span>
                </div>
                <div className="content">
                    <p>
                        La bienveillance fait partie des valeurs fondatrices de
                        Kasa. Tout comportement discriminatoire ou de
                        perturbation du voisinage entraînera une exclusion de
                        notre plateforme.
                    </p>
                </div>
            </div>

            <div className="collapse">
                <div className="collapse_title">
                    <span className="name">Service</span>
                    <span className="arrow" onClick={(e) => ColapseClicked(e)}>
                        <img src="/img/arrow.png" alt="arrow" />
                    </span>
                </div>
                <div className="content">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Etiam dignissim justo arcu, in elementum diam vulputate
                        in. Aliquam erat volutpat.
                    </p>
                </div>
            </div>

            <div className="collapse">
                <div className="collapse_title">
                    <span className="name">Responsabilité</span>
                    <span className="arrow" onClick={(e) => ColapseClicked(e)}>
                        <img src="/img/arrow.png" alt="arrow" />
                    </span>
                </div>
                <div className="content">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Etiam dignissim justo arcu, in elementum diam vulputate
                        in. Aliquam erat volutpat.
                    </p>
                </div>
            </div>
        </div>
    );
};

export default Apropos;
