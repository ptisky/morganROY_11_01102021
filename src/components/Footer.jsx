import React from "react";

const Footer = () => {
    return (
        <footer>
            <span className="logo_footer">
                K
                <img
                    src="/img/logo_white.png"
                    alt="logo kasa"
                    title="logo kasa"
                    className="logo_header"
                />
                sa
            </span>
            <p className="copy">© 2020 Kasa. All rights reserved</p>
        </footer>
    );
};

export default Footer;
