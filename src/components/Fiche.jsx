import React, { useState } from "react";
import bd from "../database/bd.json";
import { ColapseClicked } from "../js/Collapse.js";
import { Redirect } from "react-router-dom";

const Fiche = () => {
    const tags = [];
    const equipements = [];
    const stars = [];
    const carroussel = [];
    const arrow = [];
    let maxSlide;
    let currentSlide = 1;

    const [slide, curSlide] = useState(currentSlide);

    const url = window.location.href.split("/").pop();
    const itemAsked = bd.find((x) => x.id === url);

    if (itemAsked === undefined) {
        return <Redirect to="/404" />;
    }

    for (const [index, value] of itemAsked.tags.entries()) {
        tags.push(
            <li key={index} className="tag">
                {value}
            </li>
        );
    }

    for (const [index, value] of itemAsked.equipments.entries()) {
        equipements.push(<li key={index}>{value}</li>);
    }

    for (let index = 0; index < 5; index++) {
        if (index < itemAsked.rating) {
            stars.push(
                <li key={index} className="star filled">
                    <svg
                        width="30"
                        height="30"
                        viewBox="0 0 30 30"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path d="M18.645 12L15 0L11.355 12H0L9.27 18.615L5.745 30L15 22.965L24.27 30L20.745 18.615L30 12H18.645Z" />
                    </svg>
                </li>
            );
        } else {
            stars.push(
                <li key={index} className="star">
                    <svg
                        width="30"
                        height="30"
                        viewBox="0 0 30 30"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path d="M18.645 12L15 0L11.355 12H0L9.27 18.615L5.745 30L15 22.965L24.27 30L20.745 18.615L30 12H18.645Z" />
                    </svg>
                </li>
            );
        }
    }

    for (const [index, value] of itemAsked.pictures.entries()) {
        if (index === 0) {
            carroussel.push(
                <div className="item active" data-id={1} key={index + 10}>
                    <img src={value} alt={itemAsked.title} />
                </div>
            );
        } else {
            carroussel.push(
                <div className="item" data-id={index + 1} key={index + 10}>
                    <img src={value} alt={itemAsked.title} />
                </div>
            );
        }

        if (index === 0) {
            maxSlide = 1;
        } else {
            maxSlide = index - 1;
        }
    }

    const nextSlide = function (e, maxSlide) {
        e.preventDefault();
        const images = document.querySelectorAll(".car_items .item");
        let next;
        if (slide !== maxSlide) {
            next = slide + 1;
        } else {
            next = 1;
        }

        for (const image of images.values()) {
            if (parseInt(image.getAttribute("data-id")) === slide) {
                image.classList.remove("active");
                currentSlide = next;
            }

            if (parseInt(image.getAttribute("data-id")) === next) {
                image.classList.add("active");
            }
        }
        curSlide(currentSlide);
    };

    const prevSlide = function (e, maxSlide) {
        e.preventDefault();
        const images = document.querySelectorAll(".car_items .item");
        let prev;
        if (slide !== 1) {
            prev = slide - 1;
        } else {
            prev = maxSlide;
        }

        for (const image of images.values()) {
            if (parseInt(image.getAttribute("data-id")) === slide) {
                image.classList.remove("active");
                currentSlide = prev;
            }

            if (parseInt(image.getAttribute("data-id")) === prev) {
                image.classList.add("active");
            }
        }
        curSlide(currentSlide);
    };

    if (maxSlide !== 1) {
        arrow.push(
            <div key={999}>
                <img
                    src="/img/right.png"
                    alt="arrow right"
                    id="arrow_right"
                    className="arrow right"
                    key={"arr_right"}
                    onClick={(e) => nextSlide(e, maxSlide)}
                />
                <img
                    src="/img/left.png"
                    alt="arrow left"
                    id="arrow_left"
                    className="arrow left"
                    key={"arr_left"}
                    onClick={(e) => prevSlide(e, maxSlide)}
                />
            </div>
        );
    }

    return (
        <div className="fiche">
            <div className="carrousel_container">
                {arrow}
                <div className="pagination">
                    <span className="current_pag">{slide}</span>
                    <span>/</span>
                    <span className="max_pag">{maxSlide}</span>
                </div>

                <div className="car_items">{carroussel}</div>
            </div>

            <div className="container_header">
                <div className="leftSide">
                    <h2>{itemAsked.title}</h2>
                    <span className="location">{itemAsked.location}</span>

                    <ul className="tags">{tags}</ul>
                </div>

                <div className="rightSide">
                    <div className="name_container">
                        <span className="name">{itemAsked.host.name}</span>
                        <div className="img_user">
                            <img
                                src={itemAsked.host.picture}
                                alt={itemAsked.host.name}
                            />
                        </div>
                    </div>

                    <div className="stars">{stars}</div>
                </div>
            </div>

            <div className="container_desc">
                <div id="description" className="collapse">
                    <div className="collapse_title">
                        <span className="name">Desciption</span>
                        <span
                            className="arrow"
                            onClick={(e) => ColapseClicked(e)}
                        >
                            <img src="/img/arrow.png" alt="arrow" />
                        </span>
                    </div>
                    <div className="content">
                        <p>{itemAsked.description}</p>
                    </div>
                </div>

                <div id="equipements" className="collapse">
                    <div className="collapse_title">
                        <span className="name">Equipements</span>
                        <span
                            className="arrow"
                            onClick={(e) => ColapseClicked(e)}
                        >
                            <img src="/img/arrow.png" alt="arrow" />
                        </span>
                    </div>
                    <div className="content">
                        <ul>{equipements}</ul>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Fiche;
