import React from "react";
import { Link } from "react-router-dom";

const Nav = () => {
    return (
        <header>
            <h1 className="h1">
                K
                <img
                    src="/img/logo.png"
                    alt="logo kasa"
                    title="logo kasa"
                    className="logo_header"
                />
                sa
            </h1>

            <nav>
                <ul>
                    <li>
                        <Link to="/">Accueil</Link>
                    </li>
                    <li>
                        <Link to="/apropos">A Propos</Link>
                    </li>
                </ul>
            </nav>
        </header>
    );
};

export default Nav;
